# Spring AMQP Message Worker

This library provides an **AmqpMessageWorker** that enables robust handling of AMQP messages 
within a Spring Boot application. The worker also includes automatic RPC handling, which is more robust 
than the standard Spring AMQP implementation, as no channel aborts occur if a reply exchange is not available. 
Additionally, the reply mechanism is always considered optional, so your MessageListeners does not require 
any other implementation in case of RPC requests.

The library provides an AutoConfiguration for Spring Boot 3.x.x.

Example Implementation:

```java
import de.springcommons.springamqpmessageworker.AmqpMessageWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AmqpMessageListener {

    private AmqpMessageWorker amqpMessageWorker;
    private AmqpMessageService amqpMessageService;

    @RabbitListener
    public void onMessage(MessageBody messageBody, Message message) {
        amqpMessageWorker.onMessage(
                messageBody,
                message,
                amqpMessageService::doSomething,
                this::buildSuccessResponse,
                this::buildFailureResponse);
    }

    private MyResponse buildSuccessResponse(SuccessResult result){
            return MyResponse.builder()
                        .result(result)
                        .sucessful(true)
                        .exception(null)
                        .build();
    }

    private MyResponse buildFailureResponse(AmqpMessageWorkerException exception){
            return MyResponse.builder()
                        .result(null)
                        .sucessful(false)
                        .exception(exception)
                        .build();
    }
}
```
