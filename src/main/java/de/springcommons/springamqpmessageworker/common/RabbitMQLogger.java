package de.springcommons.springamqpmessageworker.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;

import java.util.Formatter;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RabbitMQLogger {

    public static void logIncomingMessage(Message message) {
        log.debug(determineMessage("Incoming message from RabbitMQ", message));
    }

    public static void logOutgoingMessage(Message message) {
        log.debug(determineMessage("Outgoing message to RabbitMQ", message));
    }

    private static String determineMessage(String title, Message message) {
        try (Formatter formatter = new Formatter()
                .format("---> %s%n", title)
                .format("Queue: %s%n", message.getMessageProperties().getConsumerQueue())
                .format("Exchange: %s%n", message.getMessageProperties().getReceivedExchange())
                .format("MessageId: %s%n", message.getMessageProperties().getMessageId())
                .format("RoutingKey: %s%n", message.getMessageProperties().getReceivedRoutingKey())
                .format("CorrelationId: %s%n", message.getMessageProperties().getCorrelationId())
                .format("Body:   %s%n", determineBody(message.getBody()))
        ) {
            return formatter.toString();
        }
    }

    private static String determineBody(byte[] messageBody) {
        return messageBody != null
               ? new String(messageBody)
               : "";
    }
}