package de.springcommons.springamqpmessageworker.common;

import org.springframework.amqp.AmqpException;

public class AmqpMessageWorkerException extends AmqpException {
    public AmqpMessageWorkerException(String message) {
        super(message);
    }

    public AmqpMessageWorkerException(Throwable throwable){
        super(throwable);
    }
}
