package de.springcommons.springamqpmessageworker;

import de.springcommons.springamqpmessageworker.common.AmqpMessageWorkerException;
import org.springframework.amqp.core.Message;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Responsible for delegating Messages, proper error handling and logging.
 */
public interface AmqpMessageWorker {

    /**
     * Default implementation for processing message without amqp reply mechanism.
     */
    <T> void onMessage(T messageBody, Message message, Consumer<T> businessConsumer);

    /**
     * Default implementation for processing message with amqp reply mechanism with {@link Consumer} as business processor.
     */
    <T, R> void onMessage(T messageBody,
                          Message message,
                          Consumer<T> businessConsumer,
                          Supplier<R> successReplyMessage,
                          Function<AmqpMessageWorkerException, R> failureReplyMessage);

    /**
     * Default implementation for processing message with amqp reply mechanism with {@link Function} as business processor.
     */
    <T, I, R> void onMessage(T messageBody,
                             Message message,
                             Function<T, I> businessFunction,
                             Function<I, R> successReplyMessage,
                             Function<AmqpMessageWorkerException, R> failureReplyMessage);
}
