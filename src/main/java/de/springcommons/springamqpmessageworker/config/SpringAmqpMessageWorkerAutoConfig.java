package de.springcommons.springamqpmessageworker.config;

import de.springcommons.springamqpmessageworker.AmqpMessageWorker;
import de.springcommons.springamqpmessageworker.AmqpReplyPublisher;
import de.springcommons.springamqpmessageworker.adapter.RabbitMQMessageWorkerAdapter;
import de.springcommons.springamqpmessageworker.adapter.RabbitMQReplyPublisherAdapter;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class SpringAmqpMessageWorkerAutoConfig {

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(RabbitTemplate.class)
    AmqpReplyPublisher amqpReplyPublisher(RabbitTemplate rabbitTemplate) {
        return new RabbitMQReplyPublisherAdapter(rabbitTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    AmqpMessageWorker amqpMessageWorker(AmqpReplyPublisher amqpReplyPublisher) {
        return new RabbitMQMessageWorkerAdapter(amqpReplyPublisher);
    }

}
