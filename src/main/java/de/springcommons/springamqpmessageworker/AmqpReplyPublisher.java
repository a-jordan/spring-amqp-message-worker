package de.springcommons.springamqpmessageworker;

import org.springframework.amqp.core.Message;

/**
 * Interface for concrete implementation of a reply mechanism via amqp.
 */
public interface AmqpReplyPublisher {

    /**
     * Mechanism for replying message via AMQP.
     *
     * @param sourceMessage incoming message which was processed before
     * @param responseBody content of response message
     * @return if successful it should return the response body
     * @param <T> typing of response body
     */
    <T> T amqpReply(Message sourceMessage, T responseBody);
}
