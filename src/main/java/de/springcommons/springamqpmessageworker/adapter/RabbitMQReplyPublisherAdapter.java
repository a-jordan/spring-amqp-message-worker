package de.springcommons.springamqpmessageworker.adapter;

import de.springcommons.springamqpmessageworker.AmqpReplyPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Address;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public class RabbitMQReplyPublisherAdapter implements AmqpReplyPublisher {

    private final RabbitTemplate rabbitTemplate;

    @Override
    public <T> T amqpReply(Message sourceMessage, T responseBody) {

        if(Objects.isNull(responseBody) || Objects.isNull(sourceMessage)){
            return null;
        }

        Address replyAddress = sourceMessage.getMessageProperties().getReplyToAddress();
        String replyQueue = sourceMessage.getMessageProperties().getReplyTo();

        if (Objects.nonNull(replyAddress)) {
            sendToReplyAddress(sourceMessage, responseBody, replyAddress);
        } else if (Objects.nonNull(replyQueue)) {
            sendToReplyQueue(sourceMessage, responseBody, replyQueue);
        }
        return responseBody;
    }

    private <T> void sendToReplyAddress(Message sourceMessage, T responseBody, Address replyAddress) {
        try {
            rabbitTemplate.convertAndSend(
                    replyAddress.getExchangeName(),
                    replyAddress.getRoutingKey(),
                    responseBody,
                    message -> {
                        message.getMessageProperties().setCorrelationId(sourceMessage.getMessageProperties().getCorrelationId());
                        return message;
                    });
        } catch (Exception e) {
            log.warn("Reply Message of Message.[CorrelationId] {} could not be send to address {}/{}",
                    sourceMessage.getMessageProperties().getCorrelationId(),
                    replyAddress.getExchangeName(),
                    replyAddress.getRoutingKey()
            );
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }

    private <R> void sendToReplyQueue(Message sourceMessage, R responseBody, String replyQueue) {
        try {
            rabbitTemplate.convertAndSend(
                    replyQueue,
                    responseBody,
                    message -> {
                        message.getMessageProperties().setCorrelationId(sourceMessage.getMessageProperties().getCorrelationId());
                        return message;
                    });
        } catch (Exception e) {
            log.warn("Reply Message of Message.[CorrelationId] {} could not be send to replyQueue {}",
                    sourceMessage.getMessageProperties().getCorrelationId(),
                    replyQueue
            );
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }
}
