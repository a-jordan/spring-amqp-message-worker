package de.springcommons.springamqpmessageworker.adapter;

import de.springcommons.springamqpmessageworker.AmqpMessageWorker;
import de.springcommons.springamqpmessageworker.AmqpReplyPublisher;
import de.springcommons.springamqpmessageworker.common.AmqpMessageWorkerException;
import de.springcommons.springamqpmessageworker.common.RabbitMQLogger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
@RequiredArgsConstructor
public class RabbitMQMessageWorkerAdapter implements AmqpMessageWorker {

    private final AmqpReplyPublisher amqpReplyPublisher;

    @Override
    public <T> void onMessage(T messageBody, Message message, Consumer<T> businessConsumer) {

        Supplier<Void> businessSupplier = () -> {
            businessConsumer.accept(messageBody);
            return null;
        };

        workMessage(message, businessSupplier, e -> null);
    }

    @Override
    public <T, R> void onMessage(T messageBody, Message message,
                                 Consumer<T> businessConsumer,
                                 Supplier<R> successReplyMessage,
                                 Function<AmqpMessageWorkerException, R> failureReplyMessage) {

        Supplier<R> businessSupplier = () -> {
            businessConsumer.accept(messageBody);
            return successReplyMessage.get();
        };

        workMessage(message, businessSupplier, failureReplyMessage);
    }

    @Override
    public <T, I, R> void onMessage(T messageBody, Message message,
                                    Function<T, I> businessFunction,
                                    Function<I, R> successReplyMessage,
                                    Function<AmqpMessageWorkerException, R> failureReplyMessage) {

        Supplier<R> businessSupplier = () -> {
            I businessResult = businessFunction.apply(messageBody);
            return successReplyMessage.apply(businessResult);
        };

        workMessage(message, businessSupplier, failureReplyMessage);
    }

    private <R> void workMessage(Message message, Supplier<R> businessSupplier, Function<AmqpMessageWorkerException, R> failureReply) {
        boolean success = true;
        R response = null;
        RabbitMQLogger.logIncomingMessage(message);

        try {
            response = businessSupplier.get();
        } catch (Exception exception) {
            success = false;
            AmqpMessageWorkerException amqpMessageWorkerException = new AmqpMessageWorkerException(exception);
            response = failureReply.apply(amqpMessageWorkerException);

            throw new AmqpRejectAndDontRequeueException(amqpMessageWorkerException);
        } finally {
            amqpReplyPublisher.amqpReply(message, response);
            if (success) {
                log.info("<--- The message from RabbitMQ was processed correctly (UUID: {}).", message.getMessageProperties().getMessageId());
            } else {
                log.warn("<--! An error occurred while processing messages from RabbitMQ (UUID: {}).", message.getMessageProperties().getMessageId());
            }
        }
    }
}